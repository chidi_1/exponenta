$(document).ready(function () {
    var timeout_link;

    if ($('.slider__el').length > 1) {
        var slider_nav = [];
        $('.slider__el').each(function () {
            slider_nav.push($(this).data('nav'));
        });

        var slider = $('.slider');

        slider.owlCarousel({
            margin: 0,
            loop: false,
            nav: true,
            navText: [,],
            dots: true,
            autoplay: false,
            items: 1,
            onInitialized: function () {
                slider.addClass('owl-carousel');
                slider.find('.owl-dot').each(function () {
                    $(this).html('<img src="' + slider_nav[$(this).index()] + '"/>');
                });
                slider.find('.owl-dot:first-child').trigger('click');
            },
            responsive: {
                0: {autoHeight: true},
                765: {autoHeight: false}
            }
        });
    }
    ;

    $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
        var tab = $(this);
        tab.addClass('active').siblings().removeClass('active')
            .closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        tab.closest('.tabs__caption').toggleClass('green pink');
        clearTimeout(timeout_link)
        change_tab();
    });

    $('ul.tabs_inner__caption').on('click', 'li:not(.active)', function () {
        var tab = $(this);
        tab.addClass('active').siblings().removeClass('active')
            .closest('.tabs_inner').find('.tabs_inner__content').removeClass('active').eq($(this).index()).addClass('active');
        var active_nav = $('.tabs__content.active .tabs_inner__caption');
        check_circle_angle(active_nav.find('li.active'));
        clearTimeout(timeout_link);
        change_tab();
    });

    function check_circle_angle(tab) {
        var index = tab.index() + 1;
        var container = tab.closest('.tabs__content').find('.circle_double');
        container.removeClass('position1 position2 position3 position4 position5 position6').addClass('position' + index)
    }

    function change_tab() {
        timeout_link = setInterval(function () {

            var active_nav = $('.tabs__content.active .tabs_inner__caption');
            var index = active_nav.find('li.active').index();
            if (index + 1 == active_nav.find('li').length) {
                active_nav.find('li').eq(0).trigger('click');
            } else {
                active_nav.find('li').eq(index + 1).trigger('click');
            }
            check_circle_angle(active_nav.find('li.active'));

        }, 10000)
    }

    change_tab();

    $('.js--toggle-menu').on('click', function () {
        $('.header').toggleClass('open');
        $('body').toggleClass('fixed');
        return false
    });

    new WOW().init();

    $('[data-fancybox]').fancybox({
        beforeClose: function () {
            var iframe = $(document).find('.fancybox-container .fancybox-iframe iframe');
            if (iframe.length) {
                src = iframe.attr('src');
                iframe.attr('src', src.replace('&autoplay=1', ''));
            }
        }
    });

    $(document).on('click', '.js--play-video', function () {
        var video = $(this).closest('.popup-video');
        var id = $(this).data('url');
        $(this).addClass('hidden-block');
        video.find('.fancybox-iframe').html(createIframe(id));
        $(document).find('.fancybox-slide--current .fancybox-iframe iframe').get(0);

        return false;
    });

    function createIframe(id) {
        var iframe = document.createElement('iframe');

        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('allow', 'autoplay');
        iframe.setAttribute('src', generateURL(id));
        iframe.classList.add('video__media');

        return iframe;
    }

    function generateURL(id) {
        //var query = '?rel=0&showinfo=0&autoplay=1';
        var query = '?rel=0&showinfo=0';

        return 'https://www.youtube.com/embed/' + id + query;
    }

});
